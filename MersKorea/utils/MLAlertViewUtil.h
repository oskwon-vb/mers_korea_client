//
//  MLAlertViewUtil.h
//  tcat4i
//
//  Created by YoungJoon Chun on 2/10/14.
//  Copyright (c) 2014 Chun, YoungJoon. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^MLAlertDelegateBlock)(UIAlertView* alert, NSInteger buttonIndex);

UIAlertView* MLAlertWithBlock(NSString* title,
        NSString* msg,
        MLAlertDelegateBlock delegateBlock,
        NSString* cancelButtonTitle,
        NSString* otherButtonTitles, ... ) NS_REQUIRES_NIL_TERMINATION;


UIAlertView* MLAlertWithTitle(NSString* title, NSString* message, ...);
#define MLAlert(msg, ...) MLAlertWithTitle(NSLocalizedString(@"Alert", @""), msg, ##__VA_ARGS__)

