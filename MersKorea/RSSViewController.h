//
//  RSSViewController.h
//  MersKorea
//
//  Created by coozplz on 2015. 6. 5..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GADBannerView;

@interface RSSViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *rssTableView;
@property GADBannerView *bannerView;


@end
