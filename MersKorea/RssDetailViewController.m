//
//  RssDetailViewController.m
//  MersKorea
//
//  Created by coozplz on 2015. 6. 6..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import "RssDetailViewController.h"
#import "MBProgressHUD.h"
#import "MMProgressHUD.h"

@interface RssDetailViewController ()

@end

@implementation RssDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.delegate = self;
    self.title = self.keyword;
    NSURL *url = [NSURL URLWithString:self.linkUrl];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [self.webView loadRequest:request];

    [MMProgressHUD setPresentationStyle:MMProgressHUDPresentationStyleFade];
    [MMProgressHUD showWithTitle:nil status:@"Loading..." cancelBlock:^{

    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)setLinkUrl:(NSString *)linkUrl {
    linkUrl = [linkUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:linkUrl];

    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
    [self.webView loadRequest:request];
    _linkUrl = linkUrl;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Error, %@", error);
}

- (void)setKeyword:(NSString *)keyword {
    _keyword = keyword;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {

}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MMProgressHUD dismiss];
}


@end
