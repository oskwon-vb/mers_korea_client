//
//  RssDetailViewController.h
//  MersKorea
//
//  Created by coozplz on 2015. 6. 6..
//  Copyright (c) 2015년 Coozplz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RssDetailViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property(nonatomic) NSString *linkUrl;
@property(nonatomic) NSString *keyword;

@end
